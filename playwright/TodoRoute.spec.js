import { test, expect } from "@playwright/test";
import AxeBuilder from "@axe-core/playwright";

//test.describe.configure({ mode: "serial" });

test.beforeEach(async ({ page }) => {
  await page.goto("/todo", { waitUntil: "networkidle" });
});

test.describe("Todo Route", () => {
  test("insert todo", async ({ page }) => {
    await page.locator("[name=todo]").fill("Hello, World!");
    await page.locator("[type=submit]").click();

    await expect(page.locator("[data-testid='todolist-id']")).toContainText(
      "Hello, World!"
    );
  });

  test("a11y", async ({ page }) => {
    const accessibilityScanResults = await new AxeBuilder({ page }).analyze();
    expect(accessibilityScanResults.violations).toEqual([]);
  });
});

// https://docs.cypress.io/api/introduction/api.html

describe("Todo List test", () => {
  it("insert todo", () => {
    cy.visit("/todo");
    cy.contains("h1", "Todo");

    cy.get("[name=todo]").type("Hello, World!");
    cy.get("[type=submit]").click();

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(2500);
    cy.get("[data-testid='todolist-id']").contains("Hello, World!");
  });

  it("a11y todo", () => {
    cy.injectAxe();
    cy.checkA11y();
  });
});

import { describe, it, expect, vi } from "vitest";
import { axe, toHaveNoViolations } from "jest-axe";

expect.extend(toHaveNoViolations);

import { mount } from "@vue/test-utils";
import VButton from "./VButton.vue";

describe("Button", () => {
  describe("a11y", () => {
    it("should have no axe violations", async () => {
      const wrapper = mount(VButton, {
        slots: { default: "Foo" },
        propsData: { onClick: () => {} },
      });

      expect(await axe(wrapper.element)).toHaveNoViolations();
    });
  });

  describe("api", () => {
    it("should render slot", async () => {
      const wrapper = mount(VButton, {
        slots: { default: "Foo" },
        propsData: { onClick: () => {} },
      });

      expect(wrapper.text()).toContain("Foo");
    });

    it("should call onClick function", async () => {
      const mockedClick = vi.fn();
      const wrapper = mount(VButton, {
        slots: { default: "Foo" },
        propsData: { onClick: mockedClick },
      });

      wrapper.trigger("click");

      expect(mockedClick).toHaveBeenCalled();
    });
  });
});

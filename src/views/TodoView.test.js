import { describe, it, expect } from "vitest";
import { axe, toHaveNoViolations } from "jest-axe";

expect.extend(toHaveNoViolations);

import { mount } from "@vue/test-utils";
import TodoView from "./TodoView.vue";

describe("TodoView", () => {
  describe("a11y", () => {
    it("should have no axe violations", async () => {
      const wrapper = mount(TodoView);

      expect(await axe(wrapper.element)).toHaveNoViolations();
    });
  });
});
